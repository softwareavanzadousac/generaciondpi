<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1 class="m-0 text-dark">Divorcios</h1>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<!-- Main row -->
		<div class="row">
			<div class="card-body">
				<div class="row">
					<div class="mensaje col-md-12">

					</div>
					<!-- 
					<div class="col-md-4">
						<label>Fecha de Matrimonio:</label>
						<label name="fechaMatrimonio" id="fechaMatrimonio"></label>
					</div>
					-->
					<div class="col-md-4">
						<label>Grupo</label>
						<select class="form-control" name="" id="url">
							<option value="http://localhost/CodeIgniter/" <?php echo (ENVIRONMENT == 'development')?'selected="selected"':''?>>ENTORNO LOCAL</option>
							<option  value="http://35.193.113.191" <?php echo (ENVIRONMENT == 'production')?'selected="selected"':''?>>Produccion</option>
							<option  value="http://35.184.41.20">Grupo 1</option>
							<option  value="http://35.239.54.7">Grupo 2</option> 
							<option  value="http://35.184.97.83">Grupo 3</option>
							<option  value="http://35.232.98.125">Grupo 5</option>
							<option  value="http://35.232.40.193">Grupo 6</option>
							<option  value="http://35.211.247.121">Grupo 7</option>

						</select>

						
					</div>
					<div class="col-md-12">

					</div>
					
					<div class="col-md-4">
						<!-- select -->
						<div class="form-group">
							<label for="dpiHombre">Hombre</label>
							<select onchange="mostrarEsposa(this.value);" class="form-control" name="hombres" id="hombres">
								<option value="">Seleccione Uno</option>
							</select>
							<input type="text" class="form-control" name="dpihombre" id="dpihombre">
							
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="dpiMujer">Mujer</label>
							<select onchange="mostrarEsposo(this.value);" class="form-control" name="mujeres" id="mujeres">
								<option value="">Seleccione Uno</option>
							</select>
							<input type="text" class="form-control" name="dpimujer" id="dpimujer">

						</div>
					</div>
					<div class="col-md-4">
						<label for="Fecha_Divorcio">Fecha</label>
						<input type="date" class="form-control"  name="Fecha_Divorcio" id="fecha">
					</div>
				</div>
				<div class="row">
					<a href="javascript:guardarDivorcio();" class="btn btn-success">Registrar Divorcio</a>
					<!-- <input type="submit" class="btn btn-success" value="Guardar"> -->
				</div>
			</div>
		</div>
		<!-- /.row (main row) -->
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
	var url = '';
	$(function(){
		url = $('#url').val();
		
		llenarHombres();
		llenarMujeres();
	});

	
	$('#url').change(function () {
		url = this.value;
		llenarHombres();
		llenarMujeres();
		//console.log(url);
	});

	$('#hombres').change(function () {
		$('#dpihombre').val(this.value);
	});
	$('#mujeres').change(function () {
		$('#dpimujer').val(this.value);
	});


	/*function cambioURL(dato){
		url= dato;
		alert(url);
	}*/

	function mostrarEsposa(dato){
		
		$.ajax({
			url: url+':9003/api/divorcio/getConyuge',
			data: {dpi: dato},
			success: function (response) {
				//alert(response.dpimujer);
				$("#mujeres option[value="+ response.dpimujer +"]").attr("selected", true);
				$('#dpimujer').val(response.dpimujer);
				//document.getElementById('fechaMatrimonio').innerHTML= response.fecha;
			}
		});
	}
	

	function mostrarEsposo(dato){
		
		$.ajax({
			url: url+':9003/api/divorcio/getConyuge2',
			data: {dpi: dato},
			success: function (response) {
				$("#hombres option[value="+ response.dpihombre +"]").attr("selected", true);
				$('#dpihombre').val(response.dpihombre);
				//document.getElementById('fechaMatrimonio').innerHTML= response.fecha;
			}
		});
	}
	function llenarHombres() {
		
		$.ajax({
			
			url: url+':9003/api/persona/getPersonas',
			data: {genero: 'M', estado_civil:'C', difunto: 0},
			success: function (response) {
				
				$('#hombres').empty();
				$('#hombres').append(new Option("Seleccione Uno",""));
				$.each(response,function (index, value) {
					$('#hombres').append(new Option(value.nombre,value.dpi));
				});
			}
		});
	}
	function llenarMujeres() {
		$.ajax({
			url: url+':9003/api/persona/getPersonas',
			data: {genero: 'F', estado_civil:'C', difunto: 0},
			success: function (response) {
				$('#mujeres').empty();
				$('#mujeres').append(new Option("Seleccione Uno",""));
				$.each(response,function (index, value) {
					$('#mujeres').append(new Option(value.nombre,value.dpi));
				});
			}
		});
	}

	function guardarDivorcio(){
		if(url=="http://35.193.113.191"){
			guardarDivorcio2();
		}else{
			guardarDivorcio1();
		}
	}

	function guardarDivorcio2() {
		var dpiHombre = $('#hombres').val();
		var dpiMujer = $('#mujeres').val();
		var Fecha_Divorcio = $('#fecha').val();
		$.ajax({
			url: url+':9003/setDivorcio',
			type: "POST",
			dataType : "json",
			data: {dpihombre: dpiHombre, dpimujer:dpiMujer, fecha: Fecha_Divorcio}
		})
		.done(function (data,textStatus,jqXHR) {
			setMsj(data);
		});
	}

	function guardarDivorcio1() {
		var dpihombre = $('#dpihombre').val();
		var dpimujer = $('#dpimujer').val();
		var fecha = $('#fecha').val();
		var grupo = $('#grupo').val();
		var rutaesb;
		if(url == "http://35.239.54.7"){
			rutaesb = url+':9006/post/comunicacionesb';
		}else{
			rutaesb = url+':10000/post/comunicacionesb';
		}


		if(url != ''){
			$.ajax({
				url: rutaesb,
				type: "POST",
				dataType : "json",
				data: {
					url	 	: url+':9003/setDivorcio',
					tipo 		: 'post',
					parametros: {
						dpihombre 	: dpihombre,
						dpimujer	: dpimujer,
						fecha		: fecha
					}
				}
			})
			.done(function (data,textStatus,jqXHR) {
				setMsj(data);
			});
		}
	}

	
	function setMsj(response) {
		var innerHtml = "";
		if(response.estado != 200){
			innerHtml 		+= "<div id='alert' name='alert' class='alert alert-success alert-dismissible fade show' role='alert'>";
			innerHtml 		+= response.mensaje;
			innerHtml 		+= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
			innerHtml 		+= "</div>";
		}else{
			innerHtml 		+= "<div id='alert' name='alert' class='alert alert-danger alert-dismissible fade show' role='alert'>";
			innerHtml 		+= response.mensaje;
			innerHtml 		+= "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
			innerHtml 		+= "</div>";
		}

		llenarHombres();
		llenarMujeres();
		$("#fecha").val('');
		$('.mensaje').html(innerHtml);
		$('#alert').fadeIn();     
  			setTimeout(function() {
       	$("#alert").fadeOut();           
  		},2000);
		

	}

	
</script>
