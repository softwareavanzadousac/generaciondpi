<?php

class MY_Model extends CI_Model
{

    protected $table;

    public function __construct()
    {
        parent::__construct();
        if (!isset($this->table)) {
            $this->table = strtolower(get_class($this));
        }
    }

    public function getAll($field = false, $sort = 'asc')
    {
        $query = $this->db
            ->from($this->table);
        if($field) {
            $query = $query->order_by($field, $sort);
        }
        return $query->get()->result();
    }

    public function getBy($field, $value,$orderColumn = 'id',$order = 'asc')
    {
        return $this->db
			->where($field, $value)
			->order_by($orderColumn,$order)
            ->get($this->table)
            ->result();
    }

    public function getDisctinct($field = '*')
    {
        return $this->db
            ->select($field)
            ->distinct()
            ->get($this->table)
            ->result();
    }

    public function getById($id, $idColumn = 'id')
    {
        return $this->db
            ->where($idColumn, $id)
            ->get($this->table)
            ->row();
    }

    public function getOneBy($field, $value)
    {
        return $this->db
            ->where($field, $value)
            ->limit(1)
            ->get($this->table)
            ->row();
    }

    public function insert($data)
    {
        $this->db
            ->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($data, $id,$campo = 'id')
    {
        $this->db
            ->where($campo, $id)
            ->update($this->table, $data);
        return $id;
    }

    public function delete($id,$idColumn = 'id')
    {
        $this->db->delete($this->table, array($idColumn => $id));
    }

    public function getSingleValueById($field, $id,$idColumn = 'id')
    {
        $row = $this->db
            ->select($field)
            ->where($idColumn, $id)
            ->get($this->table)
            ->row();
        return empty($row) ? false : $row->$field;
    }

    public function count()
    {
        $count = $this->db
            ->select('count(*) as qty')
            ->get($this->table)
            ->row();
        return $count->qty;
    }

    public function getPaged($page, $perPage)
    {
        $offset = ($page * $perPage) - $perPage;
        return $this->db
            ->get($this->table, $perPage, $offset)
            ->result();
    }
}

