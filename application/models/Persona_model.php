<?php
class Persona_model extends MY_Model
{
    protected $table = 'Persona';

    public function __construct()
    {
        parent::__construct();
	}
	
	public function checkLogin($dpi, $password)
	{
		if($dpi == '' || $password == ''){
			return 0;
		}
		return $this->db->from($this->table)->where('dpi',$dpi)->where('password',md5($password))->count_all_results();
	}

	public function count()
	{
		return $this->db->from($this->table)->count_all_results();
	}

	public function getAll($start = 0, $length = 25)
	{
		return $this->db->from($this->table.' p')
						->select("concat(p.nombres,' ',p.apellidos) nombre,p.fecha_nacimiento,p.dpi dpi, m.nombre municipio, d.nombre departamento",false)
						->join('Municipio m','m.idMunicipio = p.municipio_id')
						->join('Departamento d','d.idDepartamento = m.departamento_id')
						->limit($length,$start)->get();
	}


	

	public function getCasado($dpi)
	{
		return $this->db->from($this->table)->where('dpi',$dpi)->where('estadocivil','C')->get()->row();
	}


	public function getPersonas($genero = '',$estado_civil = '',$difunto = '')
	{
		$personas = $this->db->select('dpi, concat(nombres, " ", apellidos) as nombre',false)
							->from($this->table);
		if($genero != ''){
			$personas = $personas->where('genero',$genero);
		}
		if($estado_civil != ''){
			$personas = $personas->where_in('estadocivil',$estado_civil);
		}
		if($difunto != ''){
			$personas = $personas->where('difunto',$difunto);
		}			
							
		return $personas->where('TIMESTAMPDIFF(YEAR,fecha_nacimiento,CURDATE()) > 18')->get()->result_array();
	}

}

/*

+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| ID          | int(11)      | NO   | PRI | NULL    | auto_increment |
| AREA_TITULO | varchar(150) | NO   |     | NULL    |                |
| AREA_CLAVE  | varchar(150) | YES  |     | NULL    |                |
| AREA_STATUS | tinyint(1)   | YES  |     | 1       |                |
+-------------+--------------+------+-----+---------+----------------+

 */
