<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divorcio_test extends TestCase {

	function __construct() {
		parent::__construct();
        log_message('debug', 'Unit_tests Controller Initialized');
	}
	protected $strictRequestErrorCheck = false;

	public function test_setDivorcio_post()
	{
		$dpiHombre 			= '1621991710101';
		$dpiMujer 			= '1234567891011';
		$Fecha_Matrimonio 	= '2019-12-20';
		try {
			$output = $this->request(
				'POST', 'api/divorcio/setDivorcio', ['dpiHombre' => $dpiHombre, 'dpiMujer' => $dpiMujer, 'Fecha_Matrimonio' => $Fecha_Matrimonio]
			);
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		$result = json_decode($output);
		$this->assertEquals($result->status,false);


	}
}
