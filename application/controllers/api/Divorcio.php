<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Divorcio extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		$this->load->model('persona_model','persona');
		$this->load->model('gestion_model','gestion');
	}

	public function setDivorcio_post()
	{   
		
		$this->crudLibraries();
		$this->form_validation->set_rules('dpihombre', 'DPI del Hombre', 'trim|required');
		$this->form_validation->set_rules('dpimujer', 'DPI de la Mujer', 'trim|required');
		$this->form_validation->set_rules('fecha', 'Fecha de Divorcio', 'trim|required');
		
		if ($this->form_validation->run() === false) {
		
			return $this->response( [
				'estado' => false,
				'mensaje' => validation_errors()
			], REST_Controller::HTTP_BAD_REQUEST );
        }else{
			$hombre = $this->persona->getCasado($this->input->post('dpihombre'));
			$mujer = $this->persona->getCasado($this->input->post('dpimujer'));
			if(!isset($hombre->idPersona)){
				return $this->response( [
					'estado' => false,
					'mensaje' => 'Hombre se encuentra soltero'
				], REST_Controller::HTTP_BAD_REQUEST );

			}else if(!isset($mujer->idPersona)){
				return $this->response( [
					'estado' => false,
					'mensaje' => 'Mujer  se encuentra soltera'
				], REST_Controller::HTTP_BAD_REQUEST );
			}

			/*$casados = $this->gestion->getCasados($hombre->idPersona,$mujer->idPersona);
			
			if(!isset($casados->tipo_gestion_id)){
				return $this->response( [
					'estado' => false,
					'mensaje' => 'Personas no estan casadas entre ellas.'
				], REST_Controller::HTTP_BAD_REQUEST );
			}*/


			$this->persona->update(['estadocivil' => 'S'],$hombre->idPersona,'idPersona');
			$this->persona->update(['estadocivil' => 'S'],$mujer->idPersona,'idPersona');
			/*$this->db->where_in('persona1_id',[$hombre->idPersona,$mujer->idPersona])
						->where_in('persona2_id',[$hombre->idPersona,$mujer->idPersona])
						->update('Gestion',
						array(
							'tipo_gestion_id' => 2,
							'fecha' => 	$this->input->post('Fecha_Divorcio'),
						)
					);*/

			$this->gestion->insert(
				array(
					'tipo_gestion_id' => 2,
					'dpihombre' => $hombre->dpi,
					'dpimujer' => $mujer->dpi,
					'fecha' => 	$this->input->post('fecha'),
				)
			);
		

            return $this->response([
				'estado' => true,
				'mensaje' => 'Divorcio realizado exitosamente',
			], REST_Controller::HTTP_OK);
		}
	}

	
	public function getConyuge_get()
	{   
		$params = $this->input->get();
		$dpi = $this->input->get('dpi',true);
		$conyuge = $this->gestion->getConyuge($dpi);
		$this->response($conyuge, REST_Controller::HTTP_OK);

	}

	public function getConyuge2_get()
	{   
		$params = $this->input->get();
		$dpi = $this->input->get('dpi',true);
		$conyuge = $this->gestion->getConyuge2($dpi);
		$this->response($conyuge, REST_Controller::HTTP_OK);
	}

	private function crudLibraries()
    {
        // load classes
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
    }

		

}
