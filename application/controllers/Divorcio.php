<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Divorcio extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('persona_model','persona');
		
	}

	public function index(){
		
		$this->load->template('divorcios');
	}

	public function guardar()
	{
		
		if($this->input->server('REQUEST_METHOD')!=='POST') {
            return redirect('/divorcio', 'refresh');
		}
		$dpiHombre = $this->input->post('dpiHombre');
		$dpiMujer = $this->input->post('dpiMujer');
		$fecha = $this->input->post('Fecha_Divorcio');
		$curl_handle = curl_init();
		//print_r($dpiHombre);
		//print_r(" --> ".$dpiMujer);
		curl_setopt($curl_handle, CURLOPT_URL, base_url('api/divorcio/setDivorcio'));
		//print_r(base_url('api/Divorcio/setDivorcio'));
		//die();
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
			'dpiHombre' => $dpiHombre,
			'dpiMujer' => $dpiMujer,
			'Fecha_Divorcio' => $fecha
		));
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);
		 
		$result = json_decode($buffer);

		if($result->status){
			$this->session->set_flashdata('success_msg',$result->message);
			redirect('/divorcio');
		}else{
			$this->session->set_flashdata('error_msg',$result->message);
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
